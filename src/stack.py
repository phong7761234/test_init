class Stack:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        if isinstance(item, str):
            self.items.append(item)
        else:
            raise TypeException("Must be str")

    def pop(self):
        try:
            return self.items.pop()
        except IndexError:
            raise StackEmptyException("Stack is empty. Can't pop")

    def peek(self):
        try:
            return self.items[len(self.items) - 1]
        except IndexError:
            raise StackEmptyException("Stack is empty. Can't peek")

    def size(self):
        return len(self.items)

    def clear(self):
        self.items.clear()

    def contrains(self, value):
        return value in self.items


class StackEmptyException(Exception):
    def __init__(self, mess):
        if mess:
            self.message = mess
        else:
            self.message = None

    def __str__(self):
        return self.message

    def __repr__(self):
        return self.__str__()

    def __unicode__(self):
        return self.__str__()


class TypeException(Exception):
    def __init__(self, mess):
        if mess:
            self.message = mess
        else:
            self.message = None

    def __str__(self):
        return self.message

    def __repr__(self):
        return self.__str__()

    def __unicode__(self):
        return self.__str__()
