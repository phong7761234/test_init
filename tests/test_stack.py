import pytest
from stack import Stack, StackEmptyException, TypeException

class TestStack:

    @pytest.mark.is_empty
    def test_is_empty(self):
        stack = Stack()
        result = stack.isEmpty()
        assert result is True

        stack.push("test")
        result = stack.isEmpty()
        assert result is False

    @pytest.mark.pop
    def test_pop(self):
        stack = Stack()

        with pytest.raises(StackEmptyException):
            result = stack.pop()

        stack.push("test")
        result = stack.pop()
        assert result == "test"

    @pytest.mark.push
    def test_push(self):
        stack = Stack()

        stack.push("test")
        result = stack.pop()
        assert result == "test"

        with pytest.raises(TypeException):
            stack.push(5)

    @pytest.mark.peek
    def test_peek(self):
        stack = Stack()

        with pytest.raises(StackEmptyException):
            stack.peek()

        stack.push("test")
        result = stack.pop()
        assert result == "test"

    @pytest.mark.size
    def test_size(self):
        stack = Stack()
        assert stack.size() == 0

        stack.push("test")
        assert stack.size() == 1

        stack.pop()
        assert stack.size() == 0

    @pytest.mark.constrains
    def test_contrains(self):
        stack = Stack()
        assert stack.size() == 0

        rs = stack.contrains("test")
        assert rs == False

        stack.push("test")
        rs = stack.contrains("test")
        assert rs == True
