PYTEST:=PYTHONPATH=./src:./tests pytest
PYTEST_PARAM?= -q -W ignore::DeprecationWarning


test-all:
	@$(PYTEST) $(PYTEST_PARAM) ./tests/*.py
